﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class FutureEventsList
    {
        public List<Mover> Movers { get; } = new List<Mover>();

        public void AddMover(Mover NewMover)
        {
            Movers.Add(NewMover);
        }

        public Mover NextMover()
        {
            Mover Next = null;
            if(Movers.Count > 0)
            {
                long MinTime = Movers[0].ArrivalTime;
                foreach (Mover Move in Movers)
                {
                    if (Move.ArrivalTime < MinTime)
                    {
                        MinTime = Move.ArrivalTime;
                        Next = Move;
                    }
                    if (Move.ServiceTime < MinTime)
                    {
                        MinTime = Move.ServiceTime;
                        Next = Move;
                    }
                }
            }
            return Next;
        }

        public void currentToFuture(CurrentEventsList cel, Clock clock)
        {
            List<Mover> toDelete = new List<Mover>();
            foreach (Mover move in cel.Movers)
            {
                if (move.NextTime > clock.Timestamp)
                {
                    Movers.Add(move);
                    toDelete.Add(move);
                }
            }
            foreach (Mover move in toDelete)
            {
                cel.Movers.Remove(move);
            }
        }
    }
}
