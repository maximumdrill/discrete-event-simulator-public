﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class Engine
    {
        private int MoverAmount = 0;
        private const int DISPLAY_TIME_INTERVAL = 500;
        object MoverAmountLock = new object();

        public void run()
        {
            MoverGenerator MoveGen = new MoverGenerator();
            FutureEventsList fel = new FutureEventsList();
            CurrentEventsList cel = new CurrentEventsList();
            Clock clock = new Clock();
            Resource resource = new Resource(clock);

            AddMover();

            long displayCount = 0;
            while(true)
            {
                fel.currentToFuture(cel, clock);
                cel.RunMovers(fel);
                AddMovers(fel, MoveGen, clock, resource);
                clock.AdvanceTime(fel);
                cel.Movers.AddRange(fel.Movers);
                fel.Movers.Clear();
                if(displayCount == DISPLAY_TIME_INTERVAL)
                {
                    Console.Clear();
                    Console.Write("Average Utilization: " + resource.Utilization + 
                        " Time In Shop: " + Mover.AverageTimeInShop + 
                        "\nSimulation Time: " + clock.Timestamp + " Minutes");
                    displayCount = 0;
                }
                displayCount++;
            }
        }

        public void AddMovers(FutureEventsList fel, MoverGenerator gen, Clock clock, Resource resource)
        {
            List<Mover> AddedMovers = new List<Mover>();
            lock (MoverAmountLock)
            {
                for (int i = 0; i < MoverAmount; i++)
                {
                    Mover mover = gen.NewMover(clock);
                    fel.Movers.Add(mover);
                    AddedMovers.Add(mover);
                    mover.Run(clock, this, resource);
                }
                MoverAmount = 0;
            }
            foreach (Mover mover in AddedMovers)
            {
                mover.WaitForBlock();
            }
        }

        public void AddMover()
        {
            lock (MoverAmountLock)
            {
                MoverAmount++;
            }
        }
    }
}