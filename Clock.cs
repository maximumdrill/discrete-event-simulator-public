﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class Clock
    {
        public long Timestamp { get; set; }

        public void AdvanceTime(FutureEventsList fel)
        {
            long? Min = null;
            foreach(Mover move in fel.Movers)
            {
                if(move.NextTime > Timestamp)
                {
                    if (Min != null)
                    {
                        Min = Math.Min(move.NextTime, Min.Value);
                    }
                    else
                    {
                        Min = move.NextTime;
                    }
                }
            }
            if(Min != null)
            {
                Timestamp = Min.Value;
            }
        }
    }
}
