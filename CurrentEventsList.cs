﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class CurrentEventsList
    {
        public List<Mover> Movers { get; } = new List<Mover>();

        public void RunMovers(FutureEventsList fel)
        {
            foreach(Mover move in Movers)
            { 
                move.Unblock();
                move.WaitForBlock();
                if(!move.IsDead)
                    fel.Movers.Add(move);
            }
            Movers.Clear();
        }
    }
}
