﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// made by Noah Brown
    /// </summary>
    public class MoverGenerator
    {

        public static Random random = new Random();

        public double Expon(double mean)
        {                                
            double randomValue = random.NextDouble();
            return -(Math.Log(1 - randomValue) * mean);
        }

        public Mover NewMover(Clock clock)
        {
            Mover mover = new Mover()
            {
                ArrivalTime = (long) Expon(20) + clock.Timestamp  ,
                ServiceTime = (long) Expon(15)
            };
            return mover;
        }

    }
}
