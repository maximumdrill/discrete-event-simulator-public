﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Engine().run();
        }
    }
}
