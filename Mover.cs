﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{

    public enum MoverState { STARTING, BLOCKED, RUNNING }

    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class Mover
    {
        private bool IsAbsoluteService = false;
        private MoverState CurrentState = MoverState.STARTING;
        public long ArrivalTime { get; set; }
        public long ServiceTime { get; set; }
        public long NextTime {
            get
            {
                if (IsAbsoluteService)
                    return ServiceTime;
                else
                    return ArrivalTime;
            }
        }

        private static long totalTimeInShop = 0;
        private static int totalFinished = 0;
        public static double? AverageTimeInShop { get
            {
                if(totalFinished > 0)
                {
                    return (double) totalTimeInShop / totalFinished;
                }
                else
                {
                    return null;
                }
            }
        }
        private static object TimeInShopLock = new object();

        internal void Unblock()
        {
            CurrentState = MoverState.RUNNING;
        }

        internal void WaitForBlock()
        {
            WaitForState(MoverState.BLOCKED);
        }

        private void WaitForState(MoverState state)
        {
            while (CurrentState != state && !IsDead)
                Thread.Yield();
        }

        private void Block()
        {
            CurrentState = MoverState.BLOCKED;
            WaitForState(MoverState.RUNNING);
        }

        public bool IsDead { get; internal set; }

        internal void Run(Clock clock, Engine engine, Resource resource)
        {
            Thread thread = new Thread(() => {
                // Console.WriteLine("Waiting To Enter");
                while(ArrivalTime != clock.Timestamp)
                    Block();
                //ENtered Bshop
                // Console.WriteLine("Entered Shop");
                engine.AddMover();
                while (!resource.Lock(this))
                    Block();
                //Acquired Barber
                // Console.WriteLine("Getting haircut");
                ServiceTime += clock.Timestamp;
                IsAbsoluteService = true;
                while (ServiceTime != clock.Timestamp)
                    Block();
                // Console.WriteLine("Done!");
                resource.Dismiss();
                IsDead = true;
                lock (TimeInShopLock)
                {
                    totalTimeInShop += ServiceTime - ArrivalTime;
                    totalFinished++;
                }
            });
            thread.Start();
        }
    }
}
