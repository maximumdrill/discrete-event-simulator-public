﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscreteEventSimulation
{
    /// <summary>
    /// Made by Noah Brown
    /// </summary>
    public class Resource
    {
        public Mover mover { get; set; }
        public double? Utilization { get
            {
                if (LastTimestamp != FirstTimestamp)
                {
                    return (double)TotalUtilization / (LastTimestamp - FirstTimestamp);
                }
                else
                {
                    return null;
                }
            }
        }

        public long TotalUtilization = 0;

        private Clock clock;
        private long FirstTimestamp = 0;
        private long LastTimestamp = 0;

        public Resource(Clock _clock)
        {
            clock = _clock;
        }

        public bool Lock(Mover _Mover)
        {
            lock (this)
            {
                if(mover == null)
                {
                    LastTimestamp = clock.Timestamp;
                    mover = _Mover;
                    return true;
                }
            }
            return false;
        }

        public void Dismiss()
        {
            lock (this)
            {
                TotalUtilization += clock.Timestamp - LastTimestamp;
                LastTimestamp = clock.Timestamp;
                mover = null;
            }
        }
    }
}
